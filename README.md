# In the Lab

At the top of the page, click the "Open IDE" button. This will "Fork" the lab in GitHub, and open up an instance of "LearnIDE, " a browser-based programming environment. You will automatically be placed in the "lab root" of the IDE environment. The "lab root" is a term we'll use multiple times in this challenge, so make sure you remember this place.

Now that you are in your project directory, run `bundle install` in your terminal. This is going to load the "gems" in the Gemfile. Don't worry too much about gems right now—you'll learn about them later in the course.

<p class='util--hide'>View <a href='https://learn.co/lessons/submitting-labs-kwkp1-summer-2018'>submitting-labs-kwkp1-summer-2018</a> on Learn.co and start learning to code for free.</p>
